const express = require('express');
const cors = require('cors')
const app = express();

app.use(cors());

app.use('/login', (req, res) => {
  res.send({
    token: 'c30708f76c1bb2997b0fa23308bec8077957d7be'
  });
});

app.listen(8080, () => console.log('API is running on http://localhost:8080/login'));