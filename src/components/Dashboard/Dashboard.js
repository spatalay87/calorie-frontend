import React, {useState,useEffect} from 'react';
import './Dashboard.css'
import 'bootstrap/dist/css/bootstrap.css';
import "react-datepicker/dist/react-datepicker.css";  

import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import DatePicker from 'react-datepicker'
import { Label } from 'reactstrap';
import moment from 'moment';
import ProgressBar from 'react-bootstrap/ProgressBar';


const getToken = () => {
  const tokenString = localStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  return userToken?.token
};

export default function Dashboard() {
  const [Users,setUsers] = useState([]);
  const [consume_date,setConsume_Data] = useState([]);
  const [startDate,setstartDate] = useState("");
  const [endDate,setendDate] = useState("");
  const [startDateval,setstartDateval] = useState("");
  const [endDateval,setendDateval] = useState("");
  const history = useNavigate();

  const handleFilter = (e) =>{
    debugger;
    e.preventDefault();


  }

  // const isvalid = () =>{
  //   return startDate != "" && endDate != ""
  // }

  useEffect(()=>{
    fetch(`http://127.0.0.1:8000/consume?start_date=${startDateval}&end_date=${endDateval}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Token ${getToken()}`
      },
      // body: JSON.stringify(credentials)
    }).then(res =>{
      return res.json()
    }).then((data)=>{
      console.log(data)
      setUsers(data);
    }).catch(err =>{
      console.log(err)
    })
  },[startDate,endDate])

  useEffect(()=>{
    fetch(`http://127.0.0.1:8000/calorie-limit`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Token ${getToken()}`
      },
      // body: JSON.stringify(credentials)
    }).then(res =>{
      return res.json()
    }).then((data)=>{
      console.log(data)
      setConsume_Data(data);
    }).catch(err =>{
      console.log(err)
    })
  },[startDate,endDate])
  let cons = parseInt(((consume_date.total_limit - consume_date.available_limit)/consume_date.total_limit)*100);
  return (
    <React.Fragment>

    <h2>Items Consumed Today</h2>
    <div>
    <h3>Daily Limit Consumed Excluding Cheat Meal:</h3>
    <ProgressBar striped variant={cons > 70 ? "danger":"success"} animated now={cons} label={`${cons}%`} />
    </div>
    <br />
    <Button variant="primary" size="lg" onClick={()=> history("/add-new-entry")} active>
        Add New Entry
      </Button>
      <div className='box'>
      <div>
        <Label>Start Date</Label>
      <DatePicker selected={startDate}
      onChange={(date) => {
        setstartDate(date);
        setstartDateval(moment(date).format('YYYY-MM-DD'))}
      }
      isClearable
    />
      </div>

      <div>
        <Label>End Date</Label>
      <DatePicker selected={endDate}
      onChange={(date) => {
        setendDate(date);
        setendDateval(moment(date).format('YYYY-MM-DD'))}
      }
      isClearable
      />
      </div>
      {/* <Button variant="primary" size="lg" disabled={!isvalid()} style={{height:"100%"}} onClick={handleFilter} active>
        Filter
      </Button> */}
      </div>

<div>
    <table className="table table-bordered table-dark">
      <tr>
        <th>No.</th>
        <th>Product Name</th>
        <th>Consumed on</th>
        <th>Calorie</th>
        <th>Cheat Meal</th>
      </tr>
      {Users.map((data, key) => {
        return (
          <tr key={key}>
            <td>{key+1}</td>
            <td>{data.food.name}</td>
            <td>{data.consumed_on}</td>
            <td>{data.food.calorie_value}</td>
            <td>{data.is_cheat==1?'True':'False'}</td>
          </tr>
        )
      })}
    </table>
  </div>
  </React.Fragment>
  );
}

