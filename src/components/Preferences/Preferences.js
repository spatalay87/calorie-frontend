import React,{useState} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';


const getToken = () => {
  const tokenString = localStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  return userToken?.token
};


async function submitData(param) {
  return fetch('http://localhost:8000/consume', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token ${getToken()}`
    },
    body: JSON.stringify(param)
  })
    .then(data => data.json()).catch(err=>console.log(err))
 }

export default function Preferences() {
  const history = useNavigate();
  const [type,setType] = useState([]);
  const [inputs, setInputs] = useState({});

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.checked;
    setInputs(values => ({...values, [name]: value}))
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    let data = {
      food:{
        name:event.target[0].value,
        calorie_value: event.target[1].value,
        type: event.target[4].value
      },
      is_cheat:event.target[3].checked,
      consumed_on:event.target[2].value
    }
    await submitData(data);

    event.target.reset();
    history("/")

  }

  return(
    <div style={{ display: 'block', 
    width: 700, 
    padding: 30 }}>
<h4>Add New Consumed Item</h4>
<Form onSubmit={handleSubmit}>
<Form.Group>
<Form.Label>Enter Food name:</Form.Label>
<Form.Control type="text" 
          placeholder="Enter Food full name" />
</Form.Group>

<Form.Group>
<Form.Label>Enter the Calorie:</Form.Label>
<Form.Control type="number" placeholder="Enter the  Calorie" />
</Form.Group>

<Form.Group>
<Form.Label>Consumed On:</Form.Label>
<Form.Control type="date" placeholder="Consumed On" />
</Form.Group>

<Form.Group>
<Form.Label>Is Cheat Food:</Form.Label>
<Form.Control type="checkbox" name="is_cheat" placeholder="Is Cheat Food" onChange={handleChange} />
</Form.Group>

    <Form.Group>
        <Form.Label>Product Type:</Form.Label>
        <Form.Control
          as="select"
          selected={type}
          onChange={e => {
            console.log("e.target.value", e.target.value);
            setType(e.target.value);
          }}
        >
      <option value="1">Vegetarian</option>
      <option value="2">Non Vegetarain</option>
        </Form.Control>
      </Form.Group>


      <Form.Group>

<Button variant="primary" type="submit" style={{"marginTop":"15px"}}>
Click here to submit form
</Button>
</Form.Group>


</Form>
</div>
  );
}